# README #

A README file that explains structure of data contained in this repository


### How do I understand data in this repository? ###
Files names begining with *flux_two_cavity* contains data shown in Figures 6 and 7.
Data in these files is organised as follows:
```
* column 1 - \alpha (absorption coefficient)
* column 2 - energy flux density at Port 1
* column 3 - energy flux density at Port 2
```

File names begining with *flux_one_cavity* represent results shown in Figure 8.
Data in these files is organised as follows:
```
* column 1 - location of sources (see Table 1)
* column 2 - flux at Port 1
* column 3 - flux at side port
```

Note, that file names containing *large_ports* and *small_ports* correspond to the geometries shown
in Figure 2 and 3, respectively.
